# SatNOGS-eye-controller

This PCB is designed to control two DC motors via MOSFETs using two Raspberry Pi
GPIO pins. A DC-DC boost converter circuit is used to convert 5V input to 8V.

The design of this board is based in parts on the
[Adafruit DC & Stepper Motor HAT](https://www.adafruit.com/product/2348).

The purpose of this board is two drive the shutter mechanism of SatNOGS Eye.
To open the shutter a large current is required at the beginnning. Once the shutter
is in the "open" position, a much smaller current is sufficient to hold this state.
To close the shutter, the motor current only needs to be switched off - a mechanical
spring closes the shutter then.

To provide this functionality of a large current supply at the beginning followed
by a smaller current, a circuit consisting of a BJT transistor and a timed RC circuit
is inluded with each motor driver channel.

## Hardware Revisions

### Version v1.1.0 - 2023-03-01

- A common DC-DC step-up converter SX1308 module is used.

### Version v2.0 - 2023-03-05

- The step-up converter module was replaced by an on-board design
  (using the LM2731).

### Version v2.1 - 2023-04-06

- Fixed some errors of v2.0
- Re-routing.

## License

CERN Open Hardware Licence v1.2
